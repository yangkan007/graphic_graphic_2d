# Copyright (c) 2021-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import("//build/ohos.gni")
import("//foundation/graphic/graphic_2d/graphic_config.gni")

## Build libEGL.so {{{
config("egl_config") {
  visibility = [ ":*" ]

  defines = []

  # defines += [ "EGL_WRAPPER_DEBUG_ENABLE" ]
  if (graphic_2d_feature_use_igraphics_extend_hooks) {
    defines += [ "USE_IGRAPHICS_EXTENDS_HOOKS=1" ]
  } else {
    defines += [ "USE_IGRAPHICS_EXTENDS_HOOKS=0" ]
  }

  cflags = [
    "-Wall",
    "-Werror",
    "-g3",
  ]
}

config("egl_public_config") {
  include_dirs = [ "include" ]
  if (graphic_2d_feature_use_igraphics_extend_hooks) {
    include_dirs += egl_igraphics_extend_hooks_include_dirs
  }
}

if (current_os != "ohos") {
  group("EGL") {
    public_external_deps = [ "egl:libEGL" ]
    public_deps = [ "//third_party/openGLES:libGLES" ]
  }
} else {
  ohos_shared_library("EGL") {
    defines = [ "EGL_ERROR_ENABLE" ]
    sources = [
      "src/EGL/egl.cpp",
      "src/EGL/egl_blob_cache.cpp",
      "src/EGL/egl_core.cpp",
      "src/EGL/egl_wrapper_context.cpp",
      "src/EGL/egl_wrapper_display.cpp",
      "src/EGL/egl_wrapper_entry.cpp",
      "src/EGL/egl_wrapper_layer.cpp",
      "src/EGL/egl_wrapper_loader.cpp",
      "src/EGL/egl_wrapper_object.cpp",
      "src/EGL/egl_wrapper_surface.cpp",
      "src/thread_private_data_ctl.cpp",
    ]
    if (graphic_2d_feature_use_igraphics_extend_hooks) {
      sources += egl_igraphics_extend_hooks_sources
    }
    configs = [ ":egl_config" ]

    public_configs = [ ":egl_public_config" ]

    public_external_deps = [ "egl:libEGL" ]
    public_deps = [ "//third_party/openGLES:libGLES" ]
    external_deps = [
      "c_utils:utils",
      "hilog:libhilog",
      "init:libbeget_proxy",
      "init:libbegetutil",
    ]

    if (defined(graphic_2d_ext_configs.vendor_root)) {
      install_enable = false
    }

    output_name = "EGL"
    output_extension = "so"

    innerapi_tags = [ "platformsdk" ]
    part_name = "graphic_2d"
    subsystem_name = "graphic"
  }
}

## Build libEGL.so }}}

## Build libGLESv1.so {{{
config("glesv1_config") {
  visibility = [ ":*" ]

  cflags = [
    "-Wall",
    "-Werror",
    "-g3",
  ]
}

config("glesv1_public_config") {
  include_dirs = [ "include" ]
}

ohos_shared_library("GLESv1") {
  sources = [
    "src/GLES1/gles1.cpp",
    "src/thread_private_data_ctl.cpp",
  ]
  configs = [ ":glesv1_config" ]

  public_configs = [ ":glesv1_public_config" ]

  public_external_deps = [ "egl:libEGL" ]
  public_deps = [ "//third_party/openGLES:libGLES" ]

  external_deps = [ "hilog:libhilog" ]

  output_name = "GLESv1"
  output_extension = "so"

  part_name = "graphic_2d"
  subsystem_name = "graphic"
}

## Build libGLESv1.so }}}

## Build libGLESv2.so {{{
config("glesv2_config") {
  visibility = [ ":*" ]

  cflags = [
    "-Wall",
    "-Werror",
    "-g3",
  ]
}

config("glesv2_public_config") {
  include_dirs = [ "include" ]
}

ohos_shared_library("GLESv2") {
  sources = [
    "src/GLES2/gles2.cpp",
    "src/thread_private_data_ctl.cpp",
  ]
  configs = [ ":glesv2_config" ]

  public_configs = [ ":glesv2_public_config" ]

  public_external_deps = [ "egl:libEGL" ]
  public_deps = [ "//third_party/openGLES:libGLES" ]

  external_deps = [ "hilog:libhilog" ]

  output_name = "GLESv2"
  output_extension = "so"
  innerapi_tags = [ "ndk" ]
  part_name = "graphic_2d"
  subsystem_name = "graphic"
}

## Build libGLESv2.so }}}

## Build libGLESv3.so {{{
config("glesv3_config") {
  visibility = [ ":*" ]

  cflags = [
    "-Wall",
    "-Werror",
    "-g3",
  ]
}

config("glesv3_public_config") {
  include_dirs = [ "include" ]
}

if (current_os != "ohos") {
  group("GLESv3") {
    public_external_deps = [ "egl:libEGL" ]
    public_deps = [ "//third_party/openGLES:libGLES" ]
  }
} else {
  ohos_shared_library("GLESv3") {
    sources = [ "src/GLES3/gles3.cpp" ]
    configs = [ ":glesv3_config" ]

    public_configs = [ ":glesv3_public_config" ]

    public_external_deps = [ "egl:libEGL" ]
    public_deps = [ "//third_party/openGLES:libGLES" ]

    external_deps = [ "hilog:libhilog" ]

    if (defined(graphic_2d_ext_configs.vendor_root)) {
      install_enable = false
    }

    output_name = "GLESv3"
    output_extension = "so"

    innerapi_tags = [ "platformsdk" ]
    part_name = "graphic_2d"
    subsystem_name = "graphic"
  }
}

## Build libGLESv3.so }}}

## Build opengl_wrapper_native_test {{{
ohos_executable("opengl_wrapper_native_test") {
  install_enable = true

  sources = [ "test/opengl_wrapper_native_test.cpp" ]

  include_dirs = [
    "$graphic_2d_root/interfaces/inner_api/surface",
    "$graphic_2d_root/interfaces/inner_api/common",
    "$graphic_2d_root/rosen/modules/render_service_client",
  ]

  deps = [
    ":EGL",
    ":GLESv3",
    "$graphic_2d_root/rosen/modules/render_service_client:librender_service_client",
  ]

  external_deps = [
    "c_utils:utils",
    "hilog:libhilog",
  ]

  part_name = "graphic_2d"
  subsystem_name = "graphic"
}

## Build opengl_wrapper_native_test }}}

## Build libdebug_layer_test.so {{{
config("debug_layer_test_config") {
  visibility = [ ":*" ]

  defines = [ "EGL_WRAPPER_DEBUG_ENABLE" ]

  cflags = [
    "-Wall",
    "-Werror",
    "-g3",
  ]
}

config("debug_layer_test_public_config") {
  include_dirs = [ "include" ]
}

ohos_shared_library("debug_layer_test1") {
  sources = [ "test/debug_layer_test1.cpp" ]
  configs = [ ":debug_layer_test_config" ]

  public_configs = [ ":debug_layer_test_public_config" ]

  public_external_deps = [ "egl:libEGL" ]

  external_deps = [ "hilog:libhilog" ]

  symlink_target_name = [ "libdebug_layer_test1.so" ]

  part_name = "graphic_2d"
  subsystem_name = "graphic"
}

ohos_shared_library("debug_layer_test2") {
  sources = [ "test/debug_layer_test2.cpp" ]
  configs = [ ":debug_layer_test_config" ]

  public_configs = [ ":debug_layer_test_public_config" ]

  public_external_deps = [ "egl:libEGL" ]

  external_deps = [ "hilog:libhilog" ]

  symlink_target_name = [ "libdebug_layer_test2.so" ]

  part_name = "graphic_2d"
  subsystem_name = "graphic"
}
## Build libdebug_layer_test.so }}}
