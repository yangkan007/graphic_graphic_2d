# Copyright (c) 2024 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//foundation/arkui/ace_engine/ace_config.gni")
import("//foundation/graphic/graphic_2d/graphic_config.gni")
import("$graphic_2d_root/rosen/modules/2d_engine/rosen_text/config.gni")

import("//third_party/skia/modules/skshaper/skshaper.gni")
import("//third_party/skia/modules/skunicode/skunicode.gni")

config("skia_libtxt_config") {
  include_dirs = [
    "$graphic_2d_root/rosen/modules/2d_engine/rosen_text/export",
    "$graphic_2d_root/rosen/modules/2d_engine/rosen_text/export/skia_txt",
  ]

  cflags_cc = [
    "-Wno-implicit-fallthrough",
    "-fvisibility-inlines-hidden",
    "-std=c++17",
  ]
}

txt_root = "$rosen_root/modules/2d_engine/rosen_text/skia_txt/"

template("skia_libtxt") {
  forward_variables_from(invoker, "*")

  ohos_source_set(target_name) {
    part_name = "graphic_2d"
    subsystem_name = "graphic"
    defines += invoker.defines
    cflags_cc += invoker.cflags_cc

    public_configs = [ ":skia_libtxt_config" ]
    include_dirs = [
      "$rosen_root/modules/render_service_base/include",
      "$graphic_2d_root/rosen/modules/2d_graphics/include",
      "$graphic_2d_root/rosen/modules/2d_graphics/src",
      "$graphic_2d_root/rosen/modules/2d_graphics/src/drawing",
      "$graphic_2d_root/rosen/modules/2d_graphics/src/drawing/engine_adapter",
      "$graphic_2d_root/rosen/modules/2d_engine/rosen_text/adapter/skia_txt",
      "$graphic_2d_root/rosen/modules/2d_engine/rosen_text/skia_txt",
      "$graphic_2d_root/rosen/modules/2d_engine/rosen_text/skia_txt/txt",
      "//third_party/skia",
    ]

    sources = [
      "$txt_root/impl/drawing_painter_impl.cpp",
      "$txt_root/impl/paragraph_builder_impl.cpp",
      "$txt_root/impl/paragraph_impl.cpp",
      "$txt_root/symbol_engine/hm_symbol_node_build.cpp",
      "$txt_root/symbol_engine/hm_symbol_run.cpp",
      "$txt_root/symbol_engine/hm_symbol_txt.cpp",
      "$txt_root/txt/asset_font_manager.cpp",
      "$txt_root/txt/font_asset_provider.cpp",
      "$txt_root/txt/font_collection.cpp",
      "$txt_root/txt/paragraph_builder.cpp",
      "$txt_root/txt/paragraph_style.cpp",
      "$txt_root/txt/placeholder_run.cpp",
      "$txt_root/txt/platform.cpp",
      "$txt_root/txt/text_style.cpp",
      "$txt_root/txt/typeface_font_asset_provider.cpp",
    ]

    external_deps = []
    if (platform == "ohos" || platform == "ohos_ng") {
      external_deps += [ "init:libbegetutil" ]
      defines += [ "HM_SYMBOL_TXT_ENABLE" ]
    }

    if (defined(use_rosen_drawing) && use_rosen_drawing) {
      defines += [
        "USE_ROSEN_DRAWING",
        "USE_SKIA_TXT",
      ]
      if (ace_enable_gpu) {
        defines += [ "ACE_ENABLE_GPU" ]
      }
    }

    defines += [ "OHOS_STANDARD_SYSTEM" ]
    deps = [
      ":skia_paragraph",
      ":skia_shaper",
      ":skia_unicode",
      "//third_party/skia:skia_$platform",
    ]

    external_deps += [ "hilog:libhilog" ]
  }
}

foreach(item, ace_platforms) {
  skia_libtxt("skia_libtxt_" + item.name) {
    platform = item.name
    defines = []
    cflags_cc = []
    config = {
    }

    if (defined(item.config)) {
      config = item.config
    }

    if (defined(config.defines)) {
      defines = config.defines
    }

    if (defined(config.cflags_cc)) {
      cflags_cc = config.cflags_cc
    }
  }
}

ohos_source_set("skia_paragraph") {
  part_name = "graphic_2d"
  subsystem_name = "graphic"

  public_configs = [ ":skia_libtxt_config" ]
  include_dirs = [
    "$rosen_root/modules/render_service_base/include",
    "$graphic_2d_root/rosen/modules/2d_graphics/include",
    "$graphic_2d_root/rosen/modules/2d_graphics/src",
    "$graphic_2d_root/rosen/modules/2d_graphics/src/drawing",
    "$graphic_2d_root/rosen/modules/2d_graphics/src/drawing/engine_adapter",
    "//third_party/skia",
    "//third_party/skia/modules",
    "//third_party/skia/modules/skparagraph",
  ]
  defines = []

  sources = [
    "//third_party/skia/modules/skparagraph/src/Decorations.cpp",
    "//third_party/skia/modules/skparagraph/src/FontArguments.cpp",
    "//third_party/skia/modules/skparagraph/src/FontCollection.cpp",
    "//third_party/skia/modules/skparagraph/src/OneLineShaper.cpp",
    "//third_party/skia/modules/skparagraph/src/ParagraphBuilderImpl.cpp",
    "//third_party/skia/modules/skparagraph/src/ParagraphCache.cpp",
    "//third_party/skia/modules/skparagraph/src/ParagraphImpl.cpp",
    "//third_party/skia/modules/skparagraph/src/ParagraphPainterImpl.cpp",
    "//third_party/skia/modules/skparagraph/src/ParagraphStyle.cpp",
    "//third_party/skia/modules/skparagraph/src/Run.cpp",
    "//third_party/skia/modules/skparagraph/src/TextLine.cpp",
    "//third_party/skia/modules/skparagraph/src/TextShadow.cpp",
    "//third_party/skia/modules/skparagraph/src/TextStyle.cpp",
    "//third_party/skia/modules/skparagraph/src/TextWrapper.cpp",
    "//third_party/skia/modules/skparagraph/src/TypefaceFontProvider.cpp",
  ]

  if (defined(use_rosen_drawing) && use_rosen_drawing) {
    defines += [
      "USE_ROSEN_DRAWING",
      "USE_SKIA_TXT",
    ]
    if (ace_enable_gpu) {
      defines += [ "ACE_ENABLE_GPU" ]
    }
  }

  platform = current_os
  if (platform == "mingw") {
    platform = "windows"
  }
  deps = [
    ":skia_shaper",
    ":skia_unicode",
    "//third_party/skia:skia_$platform",
  ]

  external_deps = [ "hilog:libhilog" ]
}

ohos_source_set("skia_unicode") {
  part_name = "graphic_2d"
  subsystem_name = "graphic"

  public_configs = [ ":skia_libtxt_config" ]
  include_dirs = [
    "//third_party/skia",
    "//third_party/skia/modules",
    "//third_party/skia/modules/skunicode",
  ]
  defines = []

  public = skia_unicode_public
  defines += [ "SKUNICODE_IMPLEMENTATION=1" ]
  sources = skia_unicode_sources
  defines += [ "SK_UNICODE_AVAILABLE" ]
  sources += skia_unicode_icu_sources
  defines += [ "SK_UNICODE_ICU_IMPLEMENTATION" ]
  sources += skia_unicode_builtin_icu_sources

  platform = current_os
  if (platform == "mingw") {
    platform = "windows"
  }
  deps = [ "//third_party/skia:skia_$platform" ]
  public_deps = [ "$graphic_2d_root/rosen/build/icu:rosen_libicu_$platform" ]
}

ohos_source_set("skia_shaper") {
  part_name = "graphic_2d"
  subsystem_name = "graphic"

  public_configs = [ ":skia_libtxt_config" ]
  include_dirs = [
    "$rosen_root/modules/render_service_base/include",
    "$graphic_2d_root/rosen/modules/2d_graphics/include",
    "$graphic_2d_root/rosen/modules/2d_graphics/src",
    "$graphic_2d_root/rosen/modules/2d_graphics/src/drawing",
    "$graphic_2d_root/rosen/modules/2d_graphics/src/drawing/engine_adapter",
    "//third_party/skia",
    "//third_party/skia/modules",
    "//third_party/skia/modules/skshaper",
  ]
  defines = []

  if (defined(use_rosen_drawing) && use_rosen_drawing) {
    defines += [
      "USE_ROSEN_DRAWING",
      "USE_SKIA_TXT",
    ]
    if (ace_enable_gpu) {
      defines += [ "ACE_ENABLE_GPU" ]
    }
  }

  defines += [ "SKSHAPER_IMPLEMENTATION=1" ]
  sources = skia_shaper_primitive_sources
  sources += skia_shaper_harfbuzz_sources

  platform = current_os
  if (platform == "mingw") {
    platform = "windows"
  }
  deps = [
    ":skia_unicode",
    "//third_party/skia:skia_$platform",
  ]
}
